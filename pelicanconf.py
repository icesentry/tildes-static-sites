def page_sort_order(page):
    """Custom sorting function for the Docs pages.

    Sorts by a (manually defined) category order, then by title inside each
    individual category.
    """
    category_order = [
        "pages",
        "Instructions",
        "Philosophy",
        "Policies",
    ]

    try:
        category_index = category_order.index(page.category)
    except ValueError:
        category_index = 100

    return (category_index, page.title)


PLUGIN_PATHS = ["plugins"]
PLUGINS = ["pelican_git_modified_plugin", "wiki_url_replacer_plugin"]

AUTHOR = 'Chad Birch (Deimos)'
SITENAME = 'Tildes'
SITEURL = 'https://blog.tildes.net'
FEED_DOMAIN = 'https://blog.tildes.net'

THEME = 'theme'
TIMEZONE = 'America/Edmonton'
DEFAULT_DATE_FORMAT = '%B %-d, %Y'

# generate slugs from the filenames, not the titles
SLUGIFY_SOURCE = "basename"

# disable all slug substitutions, we can just set the exact slug with the filename
SLUG_REGEX_SUBSTITUTIONS = []

# "articles" are blog posts
ARTICLE_SAVE_AS = 'blog/{slug}.html'
ARTICLE_URL = '{slug}'

# "pages" are docs pages
PAGE_SAVE_AS = 'docs/{category}/{slug}.html'
PAGE_URL = 'https://docs.tildes.net/{category}/{slug}'

# generate an atom feed for the blog posts
FEED_ALL_ATOM = 'blog/all.atom.xml'

# specific template pages (not generated from markdown)
TEMPLATE_PAGES = {
    'index.html': 'blog/index.html',
    'page_index.html': 'docs/index.html',
}
DIRECT_TEMPLATES = ['index']

# always delete the output and generate from scratch
DELETE_OUTPUT_DIRECTORY = True

# default extension config + enable Table of Contents extension
MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.toc': {'title': 'Contents'},
    },
    'output_format': 'html5',
}

# sort the docs list using the custom sorting function
PAGE_ORDER_BY = page_sort_order

# urls for linking to page history
GITLAB_REPO_URL = 'https://gitlab.com/tildes/tildes-static-sites'
GITLAB_HISTORY_BASE_URL = GITLAB_REPO_URL + '/commits/master/content/'

# "summary" field is treated as markdown by default, disable that
FORMATTED_FIELDS = []

# don't autogenerate summary if one isn't specified
SUMMARY_MAX_LENGTH = 0

# no index page for now
INDEX_URL = ''
INDEX_SAVE_AS = ''

# don't save any of the "special" archive/etc. pages at all
DRAFT_URL = ''
DRAFT_SAVE_AS = ''
CATEGORY_URL = ''
CATEGORY_SAVE_AS = ''
TAG_URL = ''
TAG_SAVE_AS = ''
AUTHOR_URL = ''
AUTHOR_SAVE_AS = ''
ARCHIVES_SAVE_AS = ''
YEAR_ARCHIVE_SAVE_AS = ''
MONTH_ARCHIVE_SAVE_AS = ''
DAY_ARCHIVE_SAVE_AS = ''
AUTHORS_SAVE_AS = ''
CATEGORIES_SAVE_AS = ''
TAGS_SAVE_AS = ''

# disable extra feeds
CATEGORY_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
TRANSLATION_FEED_ATOM = None

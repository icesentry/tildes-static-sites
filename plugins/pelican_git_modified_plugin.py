from datetime import datetime
import subprocess

from pelican import signals


def set_modified_date_from_git(content):
    """Set the content object's modified date based on git history."""
    # get the date (YYYY-MM-DD) of the last commit that affected this file
    git_process = subprocess.run(
        ["git", "log", "-1", "--format=%cd", "--date=format:%Y-%m-%d", content.source_path],
        capture_output=True,
        text=True,
    )

    git_output = git_process.stdout.strip()

    if git_output:
        # git returned a date successfully, get the date from the output
        year, month, day = [int(part) for part in git_output.split("-")]
        modified_date = datetime(year, month, day)
    else:
        # git had no output, just use the current date
        now = datetime.now()
        modified_date = datetime(now.year, now.month, now.day)

    content.modified = modified_date
    content.locale_modified = modified_date.strftime(content.date_format)


def register():
    """Register the method to run at the end of __init__ for each page/article."""
    signals.content_object_init.connect(set_modified_date_from_git)

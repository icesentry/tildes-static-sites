Title: Hierarchical Tags Short Rules

[TOC]

Here are listed condensed versions of the tagging suggestions from the [Hierarchical Tags](https://tildes.net/~tildes.official/wiki/instructions/hierarchical_tags) page. These are intended to be easy to reference and use accordingly for users/members with tag perms and so are very concise; for the full detailing of how these tags are used, their history, and other things of that nature, see again the [main page](https://tildes.net/~tildes.official/wiki/instructions/hierarchical_tags).

# Generally group-specific tags

## ~health
### `health.` and `medicine.`
* Use `health.` and `medicine.` when there are [applicable branches](https://en.wikipedia.org/wiki/Medicine#Branches) of medicine (i.e. `medicine.veterinary`) or modifiers (i.e `health.public` and `medicine.alternative`).
* `health.mental` and `mental health` are both acceptable.

## ~humanities
### `religion.`
* Religions always take the form `[religion name].[denomination]` (i.e. `judaism.orthodox`, `christianity.catholic`, `islam.sunni`).
* Languages, where needed, take the form `[language].[dialect]` (i.e. `english.british`, `english.american`, `espanol.peninsular`, `espanol.mexicano`).

## ~science
### `socialscience.`
* When posted in science, all social science related posts take `socialscience.[discipline]` tag (i.e. `socialscience.psychology`, `socialscience.sociology`,`socialscience.anthropology`).

# Non-group specific tags

## `recurring.`
* Use `recurring.` for threads on a specific, consistent interval; otherwise, just use standalone `recurring`.

## `economics.`
* Use hierarchical tags for branches and sub-branches of economics and modifiers like `policy` and `trade` (`economics.macro` for example).
* If posting in ~science use `socialscience.` as the parent tag instead of `economics.`

## `law.`
* Use subtags whenever there's a modifier, with the modifier being the subtag (`law.labor` for example).
* If a location tag is needed, tag it separately (`law.labor` and `usa` for example).
* Do not use `law.martial` for martial law or `law.enforcement` for law enforcement/policing.

## `nsfw.` and `trigger.`
* Use `trigger.` (if desired) to specify the potentially triggering content (`trigger.rape` for example).
* Use `nsfw.` in the same way as `trigger.` for actively graphic content or links which could expose people to actively graphic content.

## `hurricanes.`, `cyclones.`, `typhoons.`
* Use the relevant parent tag for the storm, with the storm name being a subtag even if it's not a formal hurricane/typhoon/cyclone (i.e. `hurricanes.patricia`, `cyclones.tracy`, and `typhoons.yutu`).
* If the storm only has a nickname or best known name, try a truncation of that name (such as `hurricanes.1938 new england`)

Title: User Settings

[TOC]

### Passwords

[Two-factor authentication is now available](https://tildes.net/~tildes.official/536/two_factor_authentication_is_now_available)

### Selecting a theme

[You can now set a default theme for your account (and still override on individual devices if you want)](https://tildes.net/~tildes.official/5mu/you_can_now_set_a_default_theme_for_your_account_and_still_override_on_individual_devices_if_you)

### Opening links

You can choose to always open a page (linked item, comments) in either a new tab or the current tab. This is determined by your user settings.

Go to your user page by clicking on your username at the top of the sidebar on the right-hand side of the screen. In the sidebar of your user page, there is a link that says "Settings" under the Misc heading. Click on that to go to your User Settings page. Alternatively, click on [this link](https://tildes.net/settings).

On your User Settings page, look for a heading that says "**Open links in new tabs**". This has five options under it:

* Topic links to other websites
* Links to text topics and comments
* External links in topic, comment, and message text
* Links to groups on Tildes
* Links to users on Tildes 

If you tick the check box next to an option, this will automatically open those items in a new tab every time (if you tick next to "Topic links", all links will open in a new tab when you click on them). If the the check box is not ticked, those items will open in the current tab every time. You can set each option independently, or all the same.

Reference: [User settings are now available for opening links in new tabs](https://tildes.net/~tildes.official/3oi/user_settings_are_now_available_for_opening_links_in_new_tabs)

### Disable collapsing old comments when returning to a topic

By default, when you return to the comments page for a topic that has new comments since your last visit, only the new comment(s) and their parents will be expanded, with all of the old comments collapsed.

If you prefer to have all the comments stay expanded, you can un-check the "Collapse old comments when I return to a topic" option.

You can always expand collapsed comments by clicking on the "+" button on the comment stub, or the "Expand all" button at the top of the comments section.

Reference: [If you have the "mark new comments" feature enabled, old comments will now be collapsed when returning to a thread](https://tildes.net/~tildes.official/5oy/if_you_have_the_mark_new_comments_feature_enabled_old_comments_will_now_be_collapsed_when_returning)

### Bookmarks

You are able to save topics and comments for future reading and/or reference. You do this by bookmarking them.

Under every topic and comment, you will see a 'bookmark' link. Click on that to save the topic/comment to your bookmarks list.

To find a bookmarked item, go to your user page by clicking on your username at the top of the sidebar on the righthand side of the screen. In the sidebar of your user page, there is a link that says "Bookmarks" under the Misc heading. Click on that to go to your bookmarks list. Alternatively, click on [this link](https://tildes.net/bookmarks).

The bookmarked items are separated into two categories: Topics and Comments. Note that bookmarked items are displayed *in the order in which you saved them* (not in the order they were posted): the first item you bookmarked is at the bottom of the page, and the last item you bookmarked is at the top of the page.

Reference: "[Topics and comments can now be bookmarked (aka "saved")](https://tildes.net/~tildes.official/83l/topics_and_comments_can_now_be_bookmarked_aka_saved)

### User bios

Reference: [User bios added: you can write a short bio that will be visible on your user page](https://tildes.net/~tildes.official/blf/user_bios_added_you_can_write_a_short_bio_that_will_be_visible_on_your_user_page)

Title: Tildes Groups

[TOC]

### Subscribing to a group

To see topics from a group on your front page, you must subscribe to it. There are two ways to subscribe to a group:

* On the front page, look in the sidebar for the button that says 'Browse the list of groups' (at the bottom). The 'Browse groups' page lists all groups on Tildes. At the right-hand side of the screen, each group has a 'subscribe' button if you're not subscribed to it; this shows 'unsubscribe' if you are already subscribed to the group. Click on 'subscribe' to add this group to your list of subscriptions. .

* [Navigate to the group](https://tildes.net/~tildes.official/wiki/instructions/navigating_tildes#viewing_a_group) you want to subscribe to. In the sidebar is a button that says 'subscribe' (next to the number of subscribers); this shows 'unsubscribe' if you are already subscribed to the group. Click on 'subscribe'.

### Unsubscribing from a group

To no longer see topics from a group on your front page, you must unsubscribe from it. There are two ways to unsubscribe from a group:

* On the front page, look in the sidebar for the button that says 'Browse the list of groups' (at the bottom). The 'Browse groups' page lists all groups on Tildes. At the right-hand side of the screen, each group has an 'unsubscribe' button if you're subscribed to it; this shows 'subscribe' if you are not subscribed to the group. Click on 'unsubscribe' to remove this group from your list of subscriptions.

* [Navigate to the group](https://tildes.net/~tildes.official/wiki/instructions/navigating_tildes) you want to unsubscribe from. In the sidebar is a button that says 'unsubscribe' (next to the number of subscribers); this shows 'subscribe' if you are not subscribed to the group. Click on 'unsubscribe' to remove this group from your list of subscriptions.

### Creating groups

It is not currently possible for users to create their own groups on Tildes. All groups and sub-groups must be created (for now) by the creator/founder/god Deimos. Even in the future, the ability to create groups and sub-groups will be greatly restricted. It will not be open to all and sundry.

Groups and sub-groups will be determined organically, as the site grows and as the need arises. The way to demonstrate the need for a group or sub-group is to post topics with the appropriate tag in a relevant existing group or sub-group. If a particular tag gets a lot of use, that is an indication to Deimos and possible group creators that there is a need for a group for this subject.

For example:

* If you want a group for baking, post topics in ~food with a tag of "baking" (to indicate the need for a sub-group ~food.baking).

* If you want a group for horror movies, post topics in ~movies with a tag of "horror" (to indicate the need for a sub-group ~movies.horror).

* If you want a group for esports, post topics in ~games with a tag of "esports" (to indicate the need for a sub-group ~games.computer.esports).


These previous discussions provide context for this:

* [Are we going to be able to make our own groups?](https://tildes.net/~tildes/qz/are_we_going_to_be_able_to_make_our_own_groups)

* [Creating new groups?](https://tildes.net/~tildes/17o/creating_new_groups)

* [How to start a new group?](https://tildes.net/~tildes/1c2/how_to_start_a_new_group)

* [Voting on future groups rather than anyone can create anything at anytime?](https://tildes.net/~tildes/1mq/)

* [Adding new groups](https://tildes.net/~tildes/1t6/adding_new_groups)

* [Adding new groups](https://tildes.net/~tildes/2ks/adding_new_groups)

* [Users thoughts on groups?](https://tildes.net/~tildes/4z8/users_thoughts_on_groups)

* [Daily Tildes discussion - new groups added, please subscribe to them if you're interested](https://tildes.net/~tildes.official/1e1/daily_tildes_discussion_new_groups_added_please_subscribe_to_them_if_youre_interested)

* [Daily Tildes discussion - proposals for "trial groups", round 1](https://tildes.net/~tildes.official/342/daily_tildes_discussion_proposals_for_trial_groups_round_1)

* [Four new groups added (and everyone subscribed): ~anime, ~enviro, ~humanities, and ~life](https://tildes.net/~tildes.official/3qv/four_new_groups_added_and_everyone_subscribed_anime_enviro_humanities_and_life)

* [Isn't the number of groups too restrictive?](https://tildes.net/~tildes/aas/isnt_the_number_of_groups_too_restrictive)

* [When will there be a group for photographers?](https://tildes.net/~tildes/aj1/when_will_there_be_a_group_for_photographers)

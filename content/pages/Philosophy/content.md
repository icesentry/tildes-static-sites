Title: Content

[TOC]

### In-depth content (primarily text-based) is the most important

This includes linking to articles on other sites, posting text topics on Tildes itself, and the comment discussions. In general, any changes to the site that will cause "shallower" content to gain an advantage should be considered very carefully.

### Let users make their own decisions about what they want to see

Another recent trend has to been to rely heavily on machine-learning and "personalization algorithms" to determine what you see when you visit a site. These can work well, but they also often jump to wildly wrong conclusions. To quote [one of my favorite tweets on the topic](https://twitter.com/acarboni/status/976545648391553024):

> Me: *watches a single YouTube tutorial so I can fix my door hinge* 
> YouTube: WHAT'S UP, HINGE-LOVER? HERE ARE THE TOP 1000 VIDEOS FROM THE HINGER COMMUNITY THIS WEEK. CHECK OUT THIS TRENDING HINGE CONTENT FROM ENGAGING HINGEFLUENCERS

These algorithms have largely replaced predictable and chronological feeds, instead [trying to addict users by turning the experience into a slot machine](https://medium.com/thrive-global/how-technology-hijacks-peoples-minds-from-a-magician-and-google-s-design-ethicist-56d62ef5edf3#.sydedohu0) where we're never sure if we're "done" or what content we're going to be given.

On Tildes, I want to stick to predictable ways to view content, along with using additional information (such as metadata and tags) to give users flexible methods of deciding for themselves what they want to see (and not see). Once again, since Tildes doesn't need to prioritize growth or show ads, it can stay away from manipulative mechanics and focus on just helping users find what they want as easily as possible.

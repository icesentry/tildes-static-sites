Title: Policies

The rules and conditions that apply to using Tildes:

* [Privacy Policy](https://docs.tildes.net/policies/privacy-policy)

* [Code of Conduct](https://docs.tildes.net/policies/code-of-conduct)

* [Terms of Use](https://docs.tildes.net/policies/terms-of-use)
